set -e
python generator.py
gcc -g -shared -fPIC -lrt -ldl -Wall -Wextra -Wno-unused-function -Wno-unused-parameter -Wstack-protector -fstack-check fakegl.c -o libfakegl.so
export WINEPREFIX=/home/jerome/PathOfExile
#export WINEPREFIX="/home/jerome/Skyrim
export LD_PRELOAD=$PWD/libfakegl.so
cd "$WINEPREFIX/drive_c/Program Files/Grinding Gear Games/Path of Exile"
export WINEDEBUG=-d3d_shader,-d3d,-d3d_draw,-d3d_texture,-d3d_surface
wine Client.exe
#cd "$WINEPREFIX/drive_c/Program Files/The Elder Scrolls V Skyrim"
#wine TESV.exe
#chromium-bsu
#glxgears
#etracer
#SampleBrowser
#cd /home/jerome/Downloads/sanctuary/bin/
#./Sanctuary
#reset

