#include "rdtsc.h"
void glGetIntegerv(GLenum pname,GLint * params);
void glFinish();

#define CUSTOM_LOG_glTexEnvf
void log_glTexEnvf(GLenum target,  GLenum pname,  GLfloat param)
{
	char * starget = "UNKNWON";
	switch(target)
	{
	case GL_TEXTURE_ENV: starget = "GL_TEXTURE_ENV"; break;
	case GL_TEXTURE_FILTER_CONTROL:starget = "GL_TEXTURE_FILTER_CONTROL";  break;
	case GL_POINT_SPRITE: starget = "GL_POINT_SPRITE"; break;
	default: break;
	}
	gf_log("glTexEnvf(%s,...)\n", starget);
}

#define CUSTOM_LOG_glUseProgramObjectARB
static void log_glUseProgramObjectARB(GLhandleARB programObj){
	static int64_t previous = 0;
	static GLhandleARB previous_id = 0;
	glFinish();
	int64_t tsc = rdtsc();
	int64_t delta = tsc - previous;
	gf_log("%lld glUseProgramObjectARB(%X)\n", delta, previous_id);
	//gf_log("glUseProgramObjectARB(%d)\n", programObj);
	previous = rdtsc();
	previous_id = programObj;
}

#define CUSTOM_LOG_glEnable
static void log_glEnable(GLenum cap){
	char * starget = "";
    switch(cap)
	{
	case GL_TEXTURE_3D: starget="GL_TEXTURE_3D"; break;
	case GL_BLEND: starget="GL_BLEND"; break;
	case GL_SCISSOR_TEST: starget="GL_SCISSOR_TEST"; break;
	case GL_ALPHA_TEST: starget="GL_ALPHA_TEST"; break; 
	case GL_DEPTH_CLAMP: starget="GL_DEPTH_CLAMP"; break;
	case GL_DEPTH_TEST: starget="GL_DEPTH_TEST"; break;
	case GL_TEXTURE_2D: starget="GL_TEXTURE_2D"; break;
	case GL_CULL_FACE: starget="GL_CULL_FACE"; break;
	case GL_FRAGMENT_PROGRAM_ARB: starget="GL_FRAGMENT_PROGRAM_ARB"; break;
		break;
	default: break;
	}
    gf_log("glEnable(%s %X)\n", starget, cap);
}

#define CUSTOM_LOG_glBindTexture
static void log_glBindTexture(GLenum target, GLuint texture)
{
	char * starget;
	switch(target)
	{
	case GL_TEXTURE_1D: starget="GL_TEXTURE_1D";
		break;
	case GL_TEXTURE_2D: starget="GL_TEXTURE_2D";
		break;
	case GL_TEXTURE_3D: starget="GL_TEXTURE_3D";
		break;
	case GL_TEXTURE_CUBE_MAP: starget="GL_TEXTURE_CUBE_MAP";
		break;
	}
	gf_log("glBindTexture(%s, %d)\n", starget, texture);
}

#define CUSTOM_LOG_glLoadMatrixf
static void log_glLoadMatrixf(const GLfloat * m)
{
	int i;
	GLint mode;
	glGetIntegerv(GL_MATRIX_MODE, &mode);
	if(mode == GL_MODELVIEW)
	{
		gf_log("glLoadMatrixf\n");
		for(i = 0; i < 4; i++)
			gf_log("\t%g %g %g %g\n", m[i], m[i+4], m[i+8], m[i+12]);
	}
}

#define CUSTOM_LOG_glMatrixMode
static void log_glMatrixMode(GLenum mode)
{
	char * smode;
	switch(mode)
	{
	case GL_COLOR: smode="GL_COLOR";
		break;
	case GL_MODELVIEW: smode="GL_MODELVIEW";
		break;
	case GL_PROJECTION: smode="GL_PROJECTION";
		break;
	case GL_TEXTURE: smode="GL_TEXTURE";
		break;
	}
	gf_log("glMatrixMode(%s)\n", smode);
}

#define CUSTOM_LOG_glTranslatef
static void log_glTranslatef(GLfloat  x,  GLfloat  y,  GLfloat  z)
{
	gf_log("glTranslatef(%g,%g,%g)\n", x, y, z);
}

#define CUSTOM_LOG_glGenTextures
static void log_glGenTextures(GLsizei  n,  GLuint *  textures)
{
	int i;
	gf_log("glGenTextures(%d, %p)=", n, textures);
	for(i = 0; i < n; i++)
	{
		gf_log("%d ", textures[i]);
	}
	gf_log("\n");
}

#define CUSTOM_LOG_glShaderSourceARB
static void log_glShaderSourceARB(GLhandleARB shaderObj,GLsizei count,const GLcharARB* * string,const GLint * length){
	int i = 0;
    gf_log("glShaderSourceARB(%X, ", shaderObj);
	for(i = 0; i < count; i++)
		gf_log("\t%s\n", string[i]);
    gf_log("\n");
}

#define CUSTOM_LOG_glAttachObjectARB
static void log_glAttachObjectARB(GLhandleARB containerObj,GLhandleARB obj){
    gf_log("glAttachObjectARB(%X, %X)\n", containerObj, obj);
}

#define CUSTOM_LOG_glGenProgramsARB
static void log_glGenProgramsARB (GLsizei n, GLuint *programs)
{
	int i;
    gf_log("glGenProgramsARB(");
	for(i = 0; i < n; i++)
		gf_log("%d ", programs[i]);
	gf_log(")\n");
}

#define CUSTOM_LOG_glProgramStringARB
static void log_glProgramStringARB (GLenum target, GLenum format, GLsizei len, const GLvoid *string)
{
	gf_log("glProgramStringARB(%X, %X, %s)\n", target, format, string);
}

#define CUSTOM_LOG_glBindProgramARB (GLenum target, GLuint program)
static void log_glBindProgramARB (GLenum target, GLuint program)
{
	gf_log("glBindProgramARB(%X, %X)\n", target, program);
}

static void log_glCreateShaderObjectARB(GLenum shaderType, GLhandleARB r)
{
    char * stype;
    switch(shaderType)
    {
    case GL_VERTEX_SHADER: stype = "GL_VERTEX_SHADER"; break;
    case GL_FRAGMENT_SHADER: stype = "GL_FRAGMENT_SHADER"; break;
    }
    gf_log("glCreateShaderObjectARB(%s) => %X\n", stype, r);
}

