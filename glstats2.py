#! /usr/bin/env python
import sys, re
f = open(sys.argv[1], "r")
results = {}
for l in f:
	if "glGetIntegerv" in l:
		continue;
	m = re.match("([^\\s]*) ([^\\(]*)", l)
	if m:
		g = m.groups()
		pair = results.get(g[1])
		if pair == None:
			pair = (0, 0)
		v = int(g[0])/1.86E9
		results[g[1]] = (pair[0] + v, pair[1] + 1)

def key(item):
	return item[1][0]

for e in sorted(results.iteritems(), key=key):
	print e
