#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <unistd.h>
#include <sys/types.h>

static void log_glGetString(GLenum  name, const GLubyte * r)
{
	const char * sname;
	switch(name)
	{
	case GL_VENDOR: sname = "GL_VENDOR"; break;
	case GL_RENDERER: sname = "GL_RENDERER"; break;
	case GL_VERSION: sname = "GL_VERSION"; break;
	case 0x8B8C: sname = "GL_SHADING_LANGUAGE_VERSION"; break;
	case GL_EXTENSIONS: sname = "GL_EXTENSIONS"; break;
	}
	gf_log("glGetString(%s)=%s\n", sname, r);
}

static const GLubyte* real_glGetString(GLenum  name)
{
	static const GLubyte * (*fp)(GLenum) = NULL; 
	gf_dlsym("glGetString", (void**)&fp); 
	return fp(name);
}

const GLubyte* glGetString(GLenum  name)
{
	const GLubyte * r = real_glGetString(name);
	log_glGetString(name, r);
	return r; 
}

static char * shaders_status = NULL;
static unsigned int shaders_size = 0;

static void init_shaders_status(unsigned int size)
{
	int f = shm_open("fakegl-shaders", O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	if(f == -1)
	{
		gf_log("Cannot open %\n", "fakegl-shaders");
	}
	ftruncate(f, size);
	shaders_status = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, f, 0);
	if(shaders_status == MAP_FAILED)
	{
		gf_log("Cannot create mmap.\n");
	}
	shaders_size = size;
}

#ifdef CUSTOM_glUseProgramObjectARB
void glUseProgramObjectARB(GLhandleARB programObj){
	 /*if(programObj > 0x21 && programObj < 0x59)
		return;*/
	if(programObj == 0x25 || programObj == 0x35 || programObj == 0x31 || 
        programObj == 0x45 || programObj == 0x51 || programObj == 0x55 ||
        programObj == 0x59 || programObj == 0x85 || programObj == 0x89 ||
		programObj == 0x8D || programObj == 0x1FD || programObj == 0x1AD)
		return;

	{
		log_glUseProgramObjectARB(programObj);
		real_glUseProgramObjectARB(programObj);
		return;
	}
	/* if(programObj <= 1)
	{
		real_glUseProgramObjectARB(programObj);
		return;
	} */
    if(shaders_status == NULL)
		init_shaders_status(1024);
    if(programObj < shaders_size)
	{
		switch(shaders_status[programObj])
		{
		case 0:
			shaders_status[programObj] = 3;
			log_glUseProgramObjectARB(programObj);
			real_glUseProgramObjectARB(programObj);
			break;	
		case 1:
			real_glUseProgramObjectARB(programObj);
			break;
		case 2:
			break;
		case 3:
			log_glUseProgramObjectARB(programObj);
			real_glUseProgramObjectARB(programObj);
			break;
		case 4:
			log_glUseProgramObjectARB(programObj);
		}
	}
	else
    {
		init_shaders_status(shaders_size*2);
		real_glUseProgramObjectARB(programObj);
		log_glUseProgramObjectARB(programObj);
	}
}
#endif

#ifdef CUSTOM_glBindTexture
void glBindTexture(GLenum target,GLuint texture){
	//if(texture == 1)
		real_glBindTexture(target,texture);
	//log_glBindTexture(target,texture);
}
#endif

#ifdef CUSTOM_glEnable
void glEnable(GLenum cap){
	if(cap != GL_TEXTURE_3D && cap != GL_DEPTH_CLAMP && cap != GL_ALPHA_TEST)
	{
		real_glEnable(cap);
	    log_glEnable(cap);
	}
}
#endif

#ifdef CUSTOM_glTexImage3DEXT
void glTexImage3DEXT(GLenum target,GLint level,GLenum internalformat,GLsizei width,GLsizei height,GLsizei depth,GLint border,GLenum format,GLenum type,const GLvoid * pixels){
    log_glTexImage3DEXT(target,level,internalformat,width,height,depth,border,format,type,pixels);
}
#endif

#ifdef CUSTOM_glDrawElementsBaseVertex
void glDrawElementsBaseVertex(GLenum mode,GLsizei count,GLenum type,const GLvoid * indices,GLint basevertex){
    gf_log("0 %lld ", t2_glDrawElementsBaseVertex - t1_glDrawElementsBaseVertex);
    log_glDrawElementsBaseVertex(mode,count,type,indices,basevertex);
}
#endif

#ifdef CUSTOM_glUniform4fvARB
void glUniform4fvARB(GLint location,GLsizei count,const GLfloat * value){
}
#endif

static int64_t t1_glCreateShaderObjectARB, t2_glCreateShaderObjectARB;
inline static GLhandleARB real_glCreateShaderObjectARB (GLenum shaderType)
{
	static GLhandleARB (*fp_glCreateShaderObjectARB)(GLenum) = NULL;
    if(fp_glCreateShaderObjectARB == NULL)
        gf_dlsym("glCreateShaderObjectARB", (void**)&fp_glCreateShaderObjectARB);
    t1_glCreateShaderObjectARB = rdtsc();
    GLhandleARB r = fp_glCreateShaderObjectARB(shaderType);
    t2_glCreateShaderObjectARB = rdtsc();
    return r;
}

GLhandleARB glCreateShaderObjectARB (GLenum shaderType)
{
    GLhandleARB r = real_glCreateShaderObjectARB(shaderType);
    gf_log("%lld ", t1_glCreateShaderObjectARB - t2_glCreateShaderObjectARB);
    log_glCreateShaderObjectARB(shaderType, r);
    return r;
}

