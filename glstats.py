#! /usr/bin/env python

f=open("/tmp/fakegl.log")
counts = {}
for l in f:
    c = counts.get(l)
    if c == None:
        c = 0
    c = c + 1
    counts[l] = c
for e in sorted(counts.items(), key=lambda(k,v):(v,k)):
    print str(e[1])+" "+e[0][:-1]
