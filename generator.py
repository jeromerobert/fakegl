#! /usr/bin/env python
import re, os, sys

def format_proto_args(f_tokens):
    l = []
    for a in f_tokens[1]:
       if a[1]:
           l.append(a[0]+" "+a[1])
       else:
           l.append(a[0])
    return "("+",".join(l)+")"
 
def format_call_args(f_tokens):
    if f_tokens[1][0][0] == 'void':
       return "()"
    l = []
    for a in f_tokens[1]:
       l.append(a[1])
    return "("+",".join(l)+")"

def format_f_pointer(f_tokens):
    if f_tokens[1][0][0] == 'void':
       return "()"
    l = []
    for a in f_tokens[1]:
       l.append(a[0])
    return "("+",".join(l)+")"

def write_function(f_tokens, cout):
    if not f_tokens:
        return
    fname = f_tokens[0]
    cout.write("static int64_t t1_%s, t2_%s;\n" % (fname, fname));
    cout.write("static void real_"+fname+format_proto_args(f_tokens)+"""{
    static void (*fp_%(fn)s)%(fp)s = NULL;
    if(fp_%(fn)s == NULL)
        gf_dlsym("%(fn)s", (void**)&fp_%(fn)s);
    t1_%(fn)s = rdtsc();
    fp_%(fn)s%(fc)s;
    t2_%(fn)s = rdtsc();
}
""" % {"fn": fname, "fp": format_f_pointer(f_tokens), "fc":format_call_args(f_tokens)})
    cout.write("#ifndef CUSTOM_LOG_%s\n" % fname)
    cout.write("static void log_"+fname+format_proto_args(f_tokens)+"""{
    gf_log("%(fn)s\\n");
}
#endif
""" % {"fn": fname, "fc":format_call_args(f_tokens)})
    cout.write("#ifndef CUSTOM_%s\n" % fname)
    cout.write("void "+fname+format_proto_args(f_tokens)+"""{
    real_%(fn)s%(fc)s;
    gf_log("%%lld ", t2_%(fn)s - t1_%(fn)s);
    log_%(fn)s%(fc)s;
}
#endif
""" % {"fn": fname, "fc":format_call_args(f_tokens)})


func_to_keep = [
"glCreateProgram",
"glCreateProgramObjectARB",
"glAttachShader",
"glAttachObjectARB",
"glLinkProgram",
"glLinkProgramARB",
"glUserProgram", 
"glUseProgramObjectARB",
"glCreateShader",
"glCreateShaderObjectARB",
"glShaderSource",
"glShaderSourceARB",
"glCompileShader",
"glCompileShaderARB",
"glEnable",
"glBindTexture",
"glCompressedTexImage3DARB",
"glActiveTextureARB",
"glTexImage3DEXT",
"glTexEnvf",
"glXWaitGL",
"glFinish",
"glFlush",
"glDrawElementsBaseVertex",
"glDrawArrays",
"glUniform4fvARB"
]

func_to_keep = [ "glUseProgramObjectARB" ]

"""func_to_remove = [
"glBegin",
"glEnd",
"glUseProgramObjectARB",
"glCreateProgram",
"glCreateProgramObjectARB",
"glAttachShader",
"glAttachObjectARB",
"glLinkProgram",
"glLinkProgramARB",
"glUserProgram", 
"glUseProgramObjectARB",
"glCreateShader",
"glCreateShaderObjectARB",
"glShaderSource",
"glShaderSourceARB",
"glCompileShader",
"glCompileShaderARB",
"glEnable",
"glBindTexture",
"glCompressedTexImage3DARB",
"glDisable",
"glLoadIdentity",
"glActiveTextureARB",
"glMatrixMode",
"glMultiTexCoord4fARB"
]"""

already_parsed = set()
def parse_function(tokens):
    if tokens[0] != "void":
        return
    l = " ".join(tokens)
    if l.find("[16]") >= 0:
        return
    try:
        tokens.remove("APIENTRY")
    except ValueError:
        pass
    fname = tokens[1]
    args=[]
    tas = " ".join(tokens[2:]).split(",")
    for arg in tas:
       arg = arg.replace("(","").replace(")","").replace("\n"," ").strip(" ")
       ta = re.split(" +", arg)
       if ta[0]=="const" and ta[-1].find("*") >= 0:
           args.append((ta[0]+" "+ta[1]+" *", ta[2][1:]))
       elif ta[-1].find("*") >= 0:
           args.append((ta[0]+" *", ta[1][1:]))
       elif len(ta) >= 2:
           args.append((ta[0], ta[1]))
       else:
           args.append((ta[0], None))
#    if fname in func_to_remove:
#       return
    if not fname in func_to_keep:
       return
    if fname in already_parsed:
        return
    already_parsed.add(fname)
    return (fname, args)

def parse_header(filename, entry_keyword, prototypes, defines, typedefs):
    input = open(filename)
    buffer = "".join(input.readlines()).replace("\n", " \n ").replace("(", " ( ").replace(")", " ) ")
    buffer = re.split("[ \t]+", buffer)
    for i in xrange(len(buffer)):
       if buffer[i] == "\n" and buffer[i+1] == "GLAPI":
          j = 2
          proto = []
          while not buffer[i+j].endswith(";"):
             if buffer[i+j] != entry_keyword:
                 proto.append(buffer[i+j])
             j = j + 1
          proto.append(buffer[i+j][:-1])
          prototypes.append(proto)
       elif buffer[i] == "#define" and buffer[i+1].startswith("GL_"):
          defines.append((buffer[i+1], buffer[i+2]))
       elif buffer[i]=="\n" and buffer[i+1] == "typedef":
          j = 1
          typedef=[]
          while not buffer[i+j].endswith(";"):
             typedef.append(buffer[i+j])
             j = j + 1
          typedef.append(buffer[i+j])
          typedefs.append(" ".join(typedef))

prototypes=[]
defines=[]
typedefs=[]
parse_header("/usr/include/GL/gl.h", "GLAPIENTRY", prototypes, defines, typedefs)
parse_header("/usr/include/GL/glext.h", "APIENTRY", prototypes, defines, typedefs)

parsed_functions=[]
for tokens in sorted(prototypes):
    pf = parse_function(tokens)
    if pf:
        parsed_functions.append(pf)

hout = open("fakegl_proto.h", "w")
hout.write("#include <stddef.h>\n")
hout.write("#include <inttypes.h>\n")
hout.write("#define APIENTRY\n")
hout.write("#define APIENTRYP\n")
hout.write("typedef unsigned int GLhandleARB;\n")
for l in sorted(defines):
   hout.write("#define %s %s\n" % l)
for l in typedefs:
   if not l.endswith("int64_t;") and not l.endswith("int32_t;"):
       hout.write(l+"\n")
hout.close()

cout = open("fakegl.c", "w")
cout.write('#include "fakegl.h"\n')
for f in parsed_functions:
    write_function(f, cout)
cout.write('#include "fakegl_other.c"\n')
cout.write("""
typedef void (*__GLXextFuncPtr)(void);
__GLXextFuncPtr glXGetProcAddressARB (const unsigned char * v)
{
    static __GLXextFuncPtr (*real)(const unsigned char *) = NULL;
    gf_dlsym("glXGetProcAddressARB", (void**)&real);
    gf_log("glXGetProcAddressARB(%s)\\n", v);
""")
for f in parsed_functions:
    cout.write("""
    if(strcmp((const char*)v, "%(fn)s")==0) 
        return (__GLXextFuncPtr)%(fn)s; 
""" % { "fn": f[0]})
cout.write("""
    return real(v);
}""")
cout.close()
