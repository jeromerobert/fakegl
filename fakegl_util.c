#include <stdint.h>
/*static double get_time()
{
	static double start = -1.0;
	struct timeval  tv;
	gettimeofday(&tv, NULL);
	double time_in_mill = (tv.tv_sec) * 1000.0 + (tv.tv_usec) / 1000.0 ; // convert tv_sec & tv_usec to millisecond
	if(start < 0.0)
	{
		start = time_in_mill;
		return 0.0;
	}
	return time_in_mill - start;
}*/

/*__inline__ uint64_t rdtscp(void) {
    uint32_t lo, hi;
    __asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) :: "ecx" );
    return (uint64_t)hi << 32 | lo;
}*/


/*__inline__ uint64_t rdtsc() {
  uint64_t x;
  __asm__ volatile ("rdtsc" : "=A" (x));
  return x;
}*/

/*__inline__ uint64_t rdtsc() {
  uint64_t a, d;
  __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
  return (d<<32) | a;
}*/


static void gf_log(char * format, ...)
{
	static FILE * logFile = NULL;
	static int first = 1;
	static uint64_t start;
	va_list args;
	if(logFile == NULL)
		logFile = fopen("/tmp/fakegl.log", "w");
/*	if(first)
		start = rdtsc();
	fprintf(logFile, "%lld ", rdtsc()-start);*/
	va_start (args, format);
	vfprintf(logFile, format, args);
	va_end (args);
	fflush(logFile);
}

static void gf_dlsym(char * symbol, void ** handle)
{
	static void * libGLHandle = NULL;
	if(*handle == NULL)
	{
		if(libGLHandle == NULL)
			libGLHandle = dlopen("/usr/lib/i386-linux-gnu/libGL.so.1", RTLD_LAZY);
		*handle = dlsym(libGLHandle, symbol);
		if(*handle == NULL)
			gf_log("Cannot find symbol %s\n", symbol);
	}
}
