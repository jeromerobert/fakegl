/* Adapted from linux-source-3.2/arch/x86/include/asm/msr.h */
#include <stdint.h>
/*
 * both i386 and x86_64 returns 64-bit value in edx:eax, but gcc's "A"
 * constraint has different meanings. For i386, "A" means exactly
 * edx:eax, while for x86_64 it doesn't mean rdx:rax or edx:eax. Instead,
 * it means rax *or* rdx.
 */
#ifdef __x86_64__
#define DECLARE_ARGS(val, low, high)    uint32_t low, high
#define EAX_EDX_VAL(val, low, high) ((low) | ((uint64_t)(high) << 32))
#define EAX_EDX_RET(val, low, high) "=a" (low), "=d" (high)
#elif defined(__i386__)
#define DECLARE_ARGS(val, low, high)    uint64_t val
#define EAX_EDX_VAL(val, low, high) (val)
#define EAX_EDX_RET(val, low, high) "=A" (val)
#else
#error "Unknown architecture"
#endif

inline static __attribute__((always_inline))  uint64_t rdtsc(void)
{
    DECLARE_ARGS(val, low, high);
    asm volatile("rdtsc" : EAX_EDX_RET(val, low, high));
    return EAX_EDX_VAL(val, low, high);
}

