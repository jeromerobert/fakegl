#! /usr/bin/env python
import sys, re
f = open(sys.argv[1], "r")
results = {}
for l in f:
	m = re.match("(.*) glUseProgramObjectARB\\((.*)\\)", l)
	if m:
		g = m.groups()
		t = results.get(g[1])
		if t == None:
			t = 0
		v = int(g[0])/1.86E9
		if v < 1.0:
			results[g[1]] = t + v

import operator
for e in sorted(results.iteritems(), key=operator.itemgetter(1)):
	print e
